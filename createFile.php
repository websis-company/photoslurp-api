<?php 
set_time_limit(0);
ini_set('max_execution_time', 3600); //300 seconds = 5 minutes
include('connect.php');
$mysqli         = Conecta();
//$sql            = "SELECT * FROM productos WHERE status='pendiente' ORDER BY productos_id ASC";
$sql            = "SELECT * FROM productos WHERE csv = '0' AND  status = 'pendiente' ORDER BY productos_id ASC";
$search_product = $mysqli->query($sql);

$dbdata = array();
while ($products = $search_product->fetch_assoc()) {
	$id_table_db   = $products["productos_id"];
	$sku           = $products["sku"];
	$title         = $products["title"];
	$description   = $products["description"];
	$url           = $products["url"];
	$image_url     = $products["image_url"];
	$price         = $products["price"];
	$in_stock      = "in stock";
	$product_types = $products["product_types"];

	$update_product = "UPDATE productos SET csv = '1', status = 'procesado' WHERE productos_id = ".$id_table_db;
	if ($mysqli->query($update_product) === TRUE) {
		$dbdata[] = array($sku,$title,$description,$url,$image_url,$price,$in_stock,$product_types);
	}//end if
}//end while

if(count($dbdata) > 0){
     #-- Valida si existe un archivo anterior y lo borra para generar uno nuevo
    if(file_exists('productsvtex.csv')){
        unlink('productsvtex.csv');
    }//end if

    /// CSV ///
    $header  = "sku,title_es,description_es,url_es,image_url,price_mxn,in_stock_es,product_types";
    $archivo = fopen( "productsvtex.csv", "ab" );
    fputcsv( $archivo, explode(",", $header), "|" );
    foreach ($dbdata as $data_item) {
        fputcsv( $archivo, $data_item, "|" );
        # code...
    }
    fclose( $archivo );
    echo "Se creo el archivo exitosamente";
    die();
}else{
    echo "Sin información para generar archivo";
    die();
}//end if

?>