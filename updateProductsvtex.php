<?php 
set_time_limit(0);
ini_set('max_execution_time', 3600); //300 seconds = 5 minutes
include('connect.php');
$mysqli         = Conecta();
//$sql            = "SELECT * FROM productos WHERE status='pendiente' ORDER BY productos_id ASC";
$sql            = "SELECT * FROM productos WHERE csv IS NULL AND status = 'pendiente' ORDER BY productos_id ASC LIMIT 0,500";
$search_product = $mysqli->query($sql);



/** Variables de conexión a Vtex **/
$accountName = "tafmx";
$appKey      = "vtexappkey-tafmx-EEVIDI";
$appToken    = "JYMXWJKCNKWTRVGFUZEMXSZRUDIKTMJYBBLDWEZDGKROEZJXBWKAAFDQZUDSBAIAAXSPJDJPZAGMBLMUUKIXCIYVXVEBBGBFBQNHHTNYMUAZZQONIIYQSLYRYWSOYHOS";
/**
@reference : https://documenter.getpostman.com/view/845/vtex-search-api/Hs43#e8e08b8f-4036-bfa0-8196-e8267683300a 
**/
function getProductSpecificationByProductId($product_id,$accountName,$appKey,$appToken){
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://".$accountName.".vtexcommercestable.com.br/api/catalog_system/pub/products/search/?fq=productId:".$product_id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 60,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "X-VTEX-API-AppKey: ".$appKey,
            "X-VTEX-API-AppToken: ".$appToken
        ),
    ));

    $response = curl_exec($curl);
    $err      = curl_error($curl);
    curl_close($curl);

    /*if ($err) {
        echo "cURL Error #:" . $err;
    } else {*/
    $return = json_decode($response,true);
    return $return;
    //}//end if
}//end function



$i      = 0;
$dbdata = array();
while ($products = $search_product->fetch_assoc()) {
    $id_table_db           = $products["productos_id"];
    $id_product_it         = $products["id_producto"];
    $specification_product = getProductSpecificationByProductId($id_product_it,$accountName,$appKey,$appToken);

    if(!empty($specification_product)){
        #-- Estructura para CSV
        $sku         = str_replace("\"","",$specification_product[0]["productReference"]);
        $title       = str_replace("\"","",$specification_product[0]["productName"]);
        $description = str_replace("\"","",utf8_decode($specification_product[0]["description"]));
        $url         = str_replace("\"","","https://www.taf.com.mx/".$specification_product[0]["linkText"]."/p");
        $image_url   = str_replace("\"","",$specification_product[0]["items"][0]["images"][0]["imageUrl"]);
        //$price       = str_replace("\"","",$specification_product[0]["items"][0]["sellers"][0]["commertialOffer"]["ListPrice"]);
        foreach ($specification_product[0]["items"]as $value) {
            if($value["sellers"][0]["commertialOffer"]["ListPrice"] != ""){
                $price = $value["sellers"][0]["commertialOffer"]["ListPrice"];
            }else{
                $price = 0;
            }//end if
        }//end foreach

        //Categories
        $categorias     = $specification_product[0]["categories"][0];
        $arr_categorias = explode("/",$categorias);
        $levelsCat = [];
        foreach ($arr_categorias as $item) {
            if(!empty($item)){
                $levelsCat[] = $item;
            }//end if
        }//end foreach
        $strLevelsCat  = end($levelsCat);
        $in_stock      = "in stock";
        $product_types = $specification_product[0]["brand"].">".$strLevelsCat;

        if(!empty($image_url)){
            //$update_product = "UPDATE productos SET status = 'procesado' WHERE productos_id = ".$id_table_db;
            $update_product = "UPDATE productos SET sku = '".$sku."',title = '".$title."', description = '".$description."', url = '".$url."', image_url = '".$image_url."', price = '".$price."', product_types = '".$product_types."', csv = '0' WHERE productos_id = ".$id_table_db;
            if ($mysqli->query($update_product) === TRUE) {
                //$dbdata[] = array("sku"=>$sku,"title"=>($title),"description"=>($description),"url"=>$url,"image_url"=>$image_url,"price"=>$price,"in_stock"=>$in_stock);
                $dbdata[] = array($sku,$title,$description,$url,$image_url,$price,$in_stock,$product_types,"success");
            }else{
                //continue;
                $dbdata[] = array($sku,$title,$description,$url,$image_url,$price,$in_stock,$product_types,"error");
                //echo "<br> Error: " . $id_table_db. "" . $conn->error;
            }//end if
        }//end if
        
        $i++;
        // if($i > 100){
        //     break;
        // }//end if
    }else{
        $update_product = "UPDATE productos SET csv = '2', status = 'procesado' WHERE productos_id = ".$id_table_db;
        $mysqli->query($update_product);
    }//end if
}//end while


if(count($dbdata) > 0){
    echo "Datos actulizados correctamente";
}else{
    echo "Sin información para actualizar";
}//end if

/*if(count($dbdata) > 0){
    #-- Valida si existe un archivo anterior y lo borra para generar uno nuevo
    if(file_exists('productsvtex.csv')){
        unlink('productsvtex.csv');
    }

    /// CSV ///
    $header  = "sku,title_es,description_es,url_es,image_url,price_mxn,in_stock_es,product_types";
    $archivo = fopen( "productsvtex.csv", "ab" );
    fputcsv( $archivo, explode(",", $header), "|" );
    foreach ($dbdata as $data_item) {
        fputcsv( $archivo, $data_item, "|" );
        # code...
    }
    fclose( $archivo );
    echo "Se creo el archivo exitosamente";
    die();
}else{
    echo "Sin información para generar archivo";
    die();
}//end if*/
?>