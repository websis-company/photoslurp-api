<?php 
set_time_limit(0);
ini_set('max_execution_time', 3600); //300 seconds = 5 minutes
include('../connect.php');
$mysqli         = Conecta();
//$sql            = "SELECT * FROM productos WHERE status='pendiente' ORDER BY productos_id ASC";
//$sql            = "SELECT * FROM products WHERE csv = '0' AND  status = 'pendiente' ORDER BY products_id ASC";

//Verifica si todos los productos fueron actualizados
$sql            = "SELECT * FROM products WHERE status = 'pendiente'";
$search_products = $mysqli->query($sql);

//El catálogo se termino de actualizar
if($search_products->num_rows == 0){
    $sql            = "SELECT * FROM products WHERE in_stock = '1' ORDER BY products_id ASC";
    $search_product = $mysqli->query($sql);

    $dbdata = array();
    while ($products = $search_product->fetch_assoc()) {
        $id_table_db   = $products["products_id"];
        $id_vtex       = $products["id_product"];
        $sku           = $products["sku"];
        $title         = $products["title"];
        $description   = $products["description"];
        $url           = $products["url"];
        $image_url     = $products["image_url"];
        $price         = $products["price"];
        $in_stock      = $products["in_stock"] == '0' ? "out of stock" :  "in stock";
        $product_types = $products["product_types"];

        $dbdata[] = array($id_vtex,$title,$description,$url,$image_url,$price,$in_stock,$product_types);
    }//end while

    if(count($dbdata) > 0){
         #-- Valida si existe un archivo anterior y lo borra para generar uno nuevo
        if(file_exists('productsvtexV3.csv')){
            unlink('productsvtexV3.csv');
        }//end if

        /// CSV ///
        $header  = "sku,title_es,description_es,url_es,image_url,price_mxn,in_stock_es,product_types";
        $archivo = fopen( "productsvtexV3.csv", "ab" );
        fputcsv( $archivo, explode(",", $header), "|" );
        foreach ($dbdata as $data_item) {
            fputcsv( $archivo, $data_item, "|" );
            # code...
        }
        fclose( $archivo );

        $update_catalog = "UPDATE products SET status = 'pendiente'";
        $search_product = $mysqli->query($update_catalog);
        echo "Se creo el archivo exitosamente";
        die();
    }else{
        echo "Sin información para generar archivo";
        die();
    }//end if
}else{
    echo "Aún no se termina de actualizar el catálogo de productos.";
    die();
}//end if

?>