<?php 
set_time_limit(E_ALL);
ini_set('max_execution_time', 3600); //300 seconds = 5 minutes

include('core.php');
include('../connect.php');
$mysqli         = Conecta();

$sql            = "SELECT * FROM products WHERE status = 'pendiente' ORDER BY products_id ASC LIMIT 0,500";
//$sql            = "SELECT * FROM products WHERE csv IS NULL AND status = 'pendiente' ORDER BY products_id ASC";
$search_product = $mysqli->query($sql);

/** Variables de conexión a Vtex **/
$accountName = "tafmx";
$environment = "vtexcommercestable";
$appKey      = "vtexappkey-tafmx-EEVIDI";
$appToken    = "JYMXWJKCNKWTRVGFUZEMXSZRUDIKTMJYBBLDWEZDGKROEZJXBWKAAFDQZUDSBAIAAXSPJDJPZAGMBLMUUKIXCIYVXVEBBGBFBQNHHTNYMUAZZQONIIYQSLYRYWSOYHOS";
/**/

$i      = 0;
$dbdata = array();
while ($products = $search_product->fetch_assoc()) {
	$id_table_db           = $products["products_id"];
    $id_product_it         = $products["id_product"];
    $specification_product = getProductSpecificationByProductId($id_product_it,$accountName,$environment,$appKey,$appToken);

    if(!empty($specification_product)){
        #-- Estructura para CSV
        $sku         = str_replace("\"","",$specification_product[0]["productReference"]);
        $title       = str_replace("\"","",$specification_product[0]["productName"]);
        $description = str_replace("\"","",utf8_decode($specification_product[0]["description"]));
        $url         = str_replace("\"","","https://www.taf.com.mx/".$specification_product[0]["linkText"]."/p");
        $image_url   = str_replace("\"","",$specification_product[0]["items"][0]["images"][0]["imageUrl"]);
        //$price       = str_replace("\"","",$specification_product[0]["items"][0]["sellers"][0]["commertialOffer"]["ListPrice"]);
        foreach ($specification_product[0]["items"]as $value) {
            if($value["sellers"][0]["commertialOffer"]["ListPrice"] != ""){
                $price = $value["sellers"][0]["commertialOffer"]["ListPrice"];
            }else{
                $price = 0;
            }//end if
        }//end foreach

        //Categories
        $categorias     = $specification_product[0]["categories"][0];
        $arr_categorias = explode("/",$categorias);
        $levelsCat = [];
        foreach ($arr_categorias as $item) {
            if(!empty($item)){
                $levelsCat[] = $item;
            }//end if
        }//end foreach
        $strLevelsCat  = end($levelsCat);
        $in_stock      = "in stock";
        $product_types = $specification_product[0]["brand"].">".$strLevelsCat;

        if(!empty($image_url)){
            //$update_product = "UPDATE productos SET status = 'procesado' WHERE productos_id = ".$id_table_db;
            $update_product = "UPDATE products SET sku = '".$sku."',title = '".$title."', description = '".$description."', url = '".$url."', image_url = '".$image_url."', price = '".$price."', product_types = '".$product_types."', in_stock = '1', status = 'procesado' WHERE products_id = ".$id_table_db;
            if ($mysqli->query($update_product) === TRUE) {
                //$dbdata[] = array("sku"=>$sku,"title"=>($title),"description"=>($description),"url"=>$url,"image_url"=>$image_url,"price"=>$price,"in_stock"=>$in_stock);
                $dbdata[] = array($sku,$title,$description,$url,$image_url,$price,$in_stock,$product_types,"success");
            }else{
                //continue;
                $dbdata[] = array($sku,$title,$description,$url,$image_url,$price,$in_stock,$product_types,"error");
                //echo "<br> Error: " . $id_table_db. "" . $conn->error;
            }//end if
        }//end if
        
        $i++;
        // if($i > 100){
        //     break;
        // }//end if
    }else{
        $update_product = "UPDATE products SET in_stock = '0',status = 'procesado' WHERE products_id = ".$id_table_db;
        $mysqli->query($update_product);
    }//end if

	$i++;
    // if($i > 5){
    //     break;
    // }//end if
}//end while

if(count($dbdata) > 0){
    echo "Datos actulizados correctamente";
}else{
    echo "Sin información para actualizar";
}//end if


?>