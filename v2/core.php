<?php 

function getAllCategories($accountName,$environment,$seturl,$xVtexApiAppKey,$xVtexApiAppToken){
	$curl = curl_init();
	curl_setopt_array($curl, array(
		CURLOPT_URL => "https://".$accountName.".".$environment.".com.br".$seturl,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"Content-Type: application/json",
			"X-VTEX-API-AppKey:".$xVtexApiAppKey,
			"X-VTEX-API-AppToken:".$xVtexApiAppToken
		),
	));
	$response = curl_exec($curl);
	$err      = curl_error($curl);
	curl_close($curl);

	if ($err) {
		echo "cURL Error #:" . $err;
		die();
	} else {
		$resonse_array = json_decode($response,true);
		return $resonse_array;
	}//end if
}

/** Función para buscar productos paginados por categoria **/ 
function searchToVtex($id_category,$from,$to,$accountName,$appKey,$appToken){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://".$accountName.".vtexcommercestable.com.br/api/catalog_system/pvt/products/GetProductAndSkuIds?categoryId=".$id_category."&_from=".$from."&_to=".$to,
	  		CURLOPT_RETURNTRANSFER => true,
	  		CURLOPT_ENCODING => "",
	  		CURLOPT_MAXREDIRS => 10,
	  		CURLOPT_TIMEOUT => 30,
	  		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  		CURLOPT_CUSTOMREQUEST => "GET",
	  		CURLOPT_HTTPHEADER => array(
	  			"Content-Type: application/json",
				"X-VTEX-API-AppKey: ".$appKey,
				"X-VTEX-API-AppToken: ".$appToken
	  		),
	  	));

		$response = curl_exec($curl);
		$err      = curl_error($curl);
		curl_close($curl);

		$products = json_decode($response,true);
		return $products;
}//end function


/*** Función Recursiva para obtener todos los productos y variasiones de una categoria **/
function getProductsByCategory($id_category,$from,$to,$id_products,$accountName,$appKey,$appToken,$mysqli){
	$products = searchToVtex($id_category,$from,$to,$accountName,$appKey,$appToken);
	$total    = count($products["data"]);

	if($total > 0){
		foreach ($products["data"] as $key => $p_data) {
			#-- Consulta BD para saber si ya se proceso anteriormete
			// Mysqli
			$sql            = "SELECT * FROM products WHERE id_category = ".$id_category." AND id_product =".$key;
			$search_product = $mysqli->query($sql);
			if($search_product->num_rows == 0){
				//Inserta producto
				$inser_product = "INSERT INTO products (id_category,id_product,status) VALUES (".$id_category.",".$key.",'pendiente')";
				if ($mysqli->query($inser_product) === TRUE) {
					echo "<br> Insertado -->".$key;
					$id_products[$from] = $key;
					$from++;
				}//end if
			}else{
				echo "<br> ".$key." Ya existe";
			}//end if*/
		}//end foreach

		$from = $products["range"]["to"] + 1 ;
		$to   = $from + 50;
		//$it   = $it + 1;
		return getProductsByCategory($id_category,$from,$to,$id_products,$accountName,$appKey,$appToken,$mysqli);
	}else{
		$ret_array = ["id_category"=>$id_category,"products"=>$id_products];
		return $ret_array;
	}//end if
	
	//return $id_products;
}//end function

function getProductVariations($id_product,$accountName,$environment,$xVtexApiAppKey,$xVtexApiAppToken){
	$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://".$accountName.".".$environment.".com.br/api/catalog_system/pub/products/variations/".$id_product,
	  		CURLOPT_RETURNTRANSFER => true,
	  		CURLOPT_ENCODING => "",
	  		CURLOPT_MAXREDIRS => 10,
	  		CURLOPT_TIMEOUT => 30,
	  		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  		CURLOPT_CUSTOMREQUEST => "GET",
	  		CURLOPT_HTTPHEADER => array(
	  			"Content-Type: application/json",
				"X-VTEX-API-AppKey: ".$xVtexApiAppKey,
				"X-VTEX-API-AppToken: ".$xVtexApiAppToken
	  		),
	  	));

		$response = curl_exec($curl);
		$err      = curl_error($curl);
		curl_close($curl);

		$variatons = json_decode($response,true);
		return $variatons;
}//end function

function getProductSpecificationByProductId($product_id,$accountName,$environment,$xVtexApiAppKey,$xVtexApiAppToken){
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://".$accountName.".vtexcommercestable.com.br/api/catalog_system/pub/products/search/?fq=productId:".$product_id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 60,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "X-VTEX-API-AppKey: ".$xVtexApiAppKey,
            "X-VTEX-API-AppToken: ".$xVtexApiAppToken
        ),
    ));

    $response = curl_exec($curl);
    $err      = curl_error($curl);
    curl_close($curl);

    /*if ($err) {
        echo "cURL Error #:" . $err;
    } else {*/
    $return = json_decode($response,true);
    return $return;
    //}//end if
}//end function
?>