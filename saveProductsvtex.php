<?php
error_reporting(0);
set_time_limit(0);
ini_set('max_execution_time', 3600); //300 seconds = 5 minutes

include('connect.php');
$mysqli    = Conecta();

/** Variables de conexión a Vtex **/
$accountName = "tafmx";
$appKey      = "vtexappkey-tafmx-EEVIDI";
$appToken    = "JYMXWJKCNKWTRVGFUZEMXSZRUDIKTMJYBBLDWEZDGKROEZJXBWKAAFDQZUDSBAIAAXSPJDJPZAGMBLMUUKIXCIYVXVEBBGBFBQNHHTNYMUAZZQONIIYQSLYRYWSOYHOS";

/** Función para buscar productos paginados por categoria **/ 
function searchToVtex($id_category,$from,$to,$accountName,$appKey,$appToken){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://".$accountName.".vtexcommercestable.com.br/api/catalog_system/pvt/products/GetProductAndSkuIds?categoryId=".$id_category."&_from=".$from."&_to=".$to,
	  		CURLOPT_RETURNTRANSFER => true,
	  		CURLOPT_ENCODING => "",
	  		CURLOPT_MAXREDIRS => 10,
	  		CURLOPT_TIMEOUT => 30,
	  		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  		CURLOPT_CUSTOMREQUEST => "GET",
	  		CURLOPT_HTTPHEADER => array(
	  			"Content-Type: application/json",
				"X-VTEX-API-AppKey: ".$appKey,
				"X-VTEX-API-AppToken: ".$appToken
	  		),
	  	));

		$response = curl_exec($curl);
		$err      = curl_error($curl);
		curl_close($curl);

		$products = json_decode($response,true);
		return $products;
}//end function

/**
@reference : https://documenter.getpostman.com/view/845/vtex-search-api/Hs43#e8e08b8f-4036-bfa0-8196-e8267683300a 
**/
function getProductSpecificationByProductId($product_id,$accountName,$appKey,$appToken){
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://".$accountName.".vtexcommercestable.com.br/api/catalog_system/pub/products/search/?fq=productId:".$product_id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 60,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "X-VTEX-API-AppKey: ".$appKey,
            "X-VTEX-API-AppToken: ".$appToken
        ),
    ));

    $response = curl_exec($curl);
    $err      = curl_error($curl);
    curl_close($curl);

    /*if ($err) {
        echo "cURL Error #:" . $err;
    } else {*/
    $return = json_decode($response,true);
    return $return;
    //}//end if
}//end function


/*** Función Recursiva para obtener todos los productos de una categoria **/
function getProductsByCategory($id_category,$from,$to,$id_products,$accountName,$appKey,$appToken,$mysqli){
	$products = searchToVtex($id_category,$from,$to,$accountName,$appKey,$appToken);
	$total    = count($products["data"]);
	if($total > 0){
		foreach ($products["data"] as $key => $p_data) {
			#-- Consulta BD para saber si ya se proceso anteriormete
			// Mysqli
			$sql            = "SELECT * FROM productos WHERE id_categoria = ".$id_category." AND id_producto =".$key;
			$search_product = $mysqli->query($sql);
			if($search_product->num_rows == 0){
				$inser_product = "INSERT INTO productos (id_categoria,id_producto,status) VALUES (".$id_category.",".$key.",'pendiente')";
				if ($mysqli->query($inser_product) === TRUE) {
					echo "<br> Insertado -->".$key;
					$id_products[$from] = $key;
					$from++;
				}//end if
			}else{
				/*// Si ya existe y el csv  = 2  y estatus =  procesado, verifica a vtex si tiene inventario para activarlo nuevamente
				$products = $search_product->fetch_assoc();
				$csv      = $products["csv"];
				$status   = $products["status"];
				$p_id     = $products["productos_id"];

				if($csv == '2' && $status == "procesado"){//Productos sin información
					$product_detail = getProductSpecificationByProductId($key,$accountName,$appKey,$appToken); //Verifica si ya existe información para activarlo
					if(!empty($product_detail)){
						//Activa el producto para ser insertado en csv
						$sql_active_product = "UPDATE productos SET csv = 'NULL', status = 'pendiente' WHERE productos_id = ".$p_id;
						if($mysqli->query($sql_active_product) === TRUE){
							echo "<br> ".$key." Ya existe [Activado]";
						}//end if
					}else{
						echo "<br> ".$key." Ya existe [Inactivo]";
					}//end if
				}else{
					echo "<br> ".$key." Ya existe [Inactivo]";
				}//end if*/

				echo "<br> ".$key." Ya existe [Inactivo]";
			}//end if
		}//end foreach

		$from = $products["range"]["to"] + 1 ;
		$to   = $from + 50;
		//$it   = $it + 1;
		return getProductsByCategory($id_category,$from,$to,$id_products,$accountName,$appKey,$appToken,$mysqli);
	}else{
		$ret_array = ["id_category"=>$id_category,"products"=>$id_products];
		return $ret_array;
	}//end if
	
	//return $id_products;
}//end function

$curl = curl_init();
curl_setopt_array($curl, array(
	CURLOPT_URL => "http://".$accountName.".vtexcommercestable.com.br/api/catalog_system/pub/category/tree/0/",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
		"Content-Type: application/json",
		"X-VTEX-API-AppKey: ".$appKey,
		"X-VTEX-API-AppToken: ".$appToken
	),
));
$response = curl_exec($curl);
$err      = curl_error($curl);
curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	$categorias = json_decode($response,true);
}//end if


$from = 1;
$to   = 51;
$j    = 0;
$id_products       = array();
$category_products = array();
$ids               = array();
foreach ($categorias as $categoria_item) {
	$ids[] = getProductsByCategory($categoria_item["id"],$from,$to,$id_products,$accountName,$appKey,$appToken,$mysqli);
}//end foreach

